module.exports = function (ctx) {
    var Q = ctx.requireCordovaModule('q');
    var fs = ctx.requireCordovaModule('fs');
    var path = ctx.requireCordovaModule('path');

    var variableRegex = /--variable (\w+)=(.+?)(?= |$)/g;
    var availableConfigKeys = ['location', 'manifestUrlPath', 'indexFilePath'];
    var updaterConfig = {'location': 'http://updater.ideascup.me/demo'};
    var match;
    while ((match = variableRegex.exec(ctx.cmdLine))) {
        var key = match[1].toLowerCase().replace(/_(\w)/g, function (m, m1) {
            return m1.toUpperCase();
        });
        
        if (availableConfigKeys.indexOf(key) > -1) {
            updaterConfig[key] = match[2];
        }
    }

    console.log('Copy default files');
    var updaterViewFileName = 'updater.html';
    var configFilePath = path.join(ctx.opts.projectRoot, 'config.xml');
    var pluginMainDirectory = path.join(ctx.opts.plugin.pluginInfo.dir, 'www');
    var projectMainDirectory = path.join(ctx.opts.projectRoot, 'www');
    
    var fileNames = [updaterViewFileName, 'updaterconfig.json', 'config.xml'];
    var promises = fileNames.map(function (fileName, i) {
        var deferred = Q.defer();
        
        if (i === 0) {
            var rs = fs.createReadStream(path.join(pluginMainDirectory, fileName));
            var ws = fs.createWriteStream(path.join(projectMainDirectory, fileName));
            rs.on('error', deferred.reject);
            ws.on('error', deferred.reject);
            ws.on('finish', function () {
                console.log('\033[1;32m' + fileName + '\033[0m -> ' + projectMainDirectory);
                deferred.resolve();
            });
            rs.pipe(ws);
        } else if (i === 1) {
            var filePath = path.join(projectMainDirectory, fileName);
            fs.writeFile(filePath, JSON.stringify(updaterConfig, null, 4), 'utf8', function (err) {
                if (err) {
                    return deferred.reject(err);
                }
                
                console.log('\033[1;32m' + fileName + '\033[0m -> ' + projectMainDirectory);
                deferred.resolve();
            });
        } else if (i === 2) {
            fs.readFile(configFilePath, 'utf8', function (err, content) {
                if (err) {
                    return deferred.reject(err);
                }
    
                console.log('<content src="\033[1;32m' + updaterViewFileName + '\033[0m"/> -> ' + configFilePath);
                var match = /<content src="(.*?)"/i.exec(content);
                if (match) {
                    content = content.replace(match[0], '<content src="' + updaterViewFileName + '"');
                } else {
                    content = content.replace(/(<\/widget>)/i, '    <content src="' + updaterViewFileName + '" />\n$1');
                }
                
                fs.writeFile(configFilePath, content, function (err) {
                    if (err) {
                        deferred.reject(err);
                    } else {
                        deferred.resolve();
                    }
                });
            });
        }
        
        return deferred.promise;
    });

    var deferred = Q.defer();
    
    Q.all(promises)
        .then(deferred.resolve)
        .catch(deferred.reject);

    return deferred.promise;
};