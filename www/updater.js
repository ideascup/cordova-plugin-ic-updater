/*global FileTransfer*/

var defaultUpdaterConfig = {
    'location': "http://updater.ideascup.me/demo",
    'manifestUrlPath': '/manifest.json',
    'indexFilePath': 'index.html'
};


var fs = {
    '_fs': null,
    
    getRootDirPath: function () {
        return this._fs.root.toURL();
    },
    
    getFileNameByPath: function (filePath) {
        return filePath.split('/').slice(-1)[0];
    },
    
    requestFileSystem: function (callback) {
        var that = this;
        window.requestFileSystem(
            window.LocalFileSystem.PERSISTENT, 0,
            function (fs) {
                that._fs = fs;
                callback(null, fs);
            },
            callback);
    },
    
    readFile: function (filePath, defaultFileContent, callback) {
        this._fs.root.getFile(filePath, {'create': false}, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader;
                reader.onloadend = function (evt) {
                    callback(null, evt.target.result);
                };
                reader.onerror = callback;
                reader.readAsText(file);
            });
        }, function (err) {
            if (err.code === 1 && defaultFileContent) {
                var content = typeof defaultFileContent === 'object' ?
                    JSON.stringify(defaultFileContent)
                    : defaultFileContent;
                callback(null, content);
            } else {
                callback(err);
            }
        });
    },
    
    saveFile: function (filePath, fileData, callback) {
        this._fs.root.getFile(filePath, {'create': true}, function (fileEntry) {
            fileEntry.createWriter(function (writer) {
                writer.onwriteend = function () { callback(); };
                writer.onerror = callback;
                writer.write(fileData);
            });
        }, callback);
    },
    
    downloadFile: function (fileUri, filePath, callback) {
        var fileName = this.getFileNameByPath(filePath);
        
        this._fs.root.getFile(fileName, {'create': true}, function (fileEntry) {
            new FileTransfer().download(
                encodeURI(fileUri),
                fileEntry.toURL().replace(new RegExp(fileName + '$'), filePath),
                function () { callback(null) },
                callback,
                false);
        }, callback);
    }
};


var _config = {};
var _manifest = {};

var Updater = {
    request: function (url, callback) {
        var xhr = new XMLHttpRequest;
        
        xhr.open('GET', url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) {
                return;
            }
            
            if (xhr.status !== 200) {
                callback(new Error(xhr.statusText));
                return;
            }
            
            callback(null, xhr.responseText);
        };
        
        xhr.send(null);
    },
    
    loadApp: function () {
        var redirectPath = fs.getRootDirPath() + _config.indexFilePath;
        window.location.href = redirectPath;
    },
    
    initialize: function () {
        var that = this;
        document.addEventListener('deviceready', function () {
            fs.requestFileSystem(function (err) {
                if (err) {
                    return alert(JSON.stringify(err));
                }
                
                var currentFileName = fs.getFileNameByPath(window.location.href);
                var rootUrl = window.location.href.slice(0, currentFileName.length * -1);
                var configUrl = rootUrl + 'updaterconfig.json';
                
                that.request(configUrl, function (err, content) {
                    if (err) {
                        return alert(JSON.stringify(err));
                    }
                    
                    _config = Object.assign({}, defaultUpdaterConfig, JSON.parse(content));
                    // cordova.js hack :) v2.0 support reload plugins
                    fs.saveFile('cordova.js', "var s=document.createElement('script');var s2=document.querySelector('script[src*=\"cordova.js\"]');s2.parentNode.insertBefore(s,s2.nextSibling);s.src='" + rootUrl + "cordova.js'");
                    
                    var defaultManifest = {
                        'version': 0,
                        'files': []
                    };
                    
                    fs.readFile('manifest.json', defaultManifest, function (err, content) {
                        if (err) {
                            return alert(JSON.stringify(err));
                        }
                        
                        _manifest = JSON.parse(content);
                        
                        var params = [
                            '_=' + Math.random(),
                            'version=' + _manifest.version
                        ];
                        
                        var manifestUrl = _config.location + _config.manifestUrlPath + '?' + params.join('&');
                        
                        that.request(manifestUrl, function (err, latestManifestContent) {
                            if (err) {
                                return alert(JSON.stringify(err));
                            }
                            
                            var manifest = JSON.parse(latestManifestContent);
                            if (manifest.version === _manifest.version) {
                                return that.loadApp();
                            }
                            
                            var count = manifest.files.length;
                            
                            function next(err) {
                                if (err) {
                                    return alert(JSON.stringify(err));
                                }
                                
                                if (count-- === 0) {
                                    that.loadApp();
                                }
                                
                                var filePath = manifest.files[count];
                                var fileUrl = _config.location + '/' + filePath;
                                fs.downloadFile(fileUrl, filePath, next);
                            }
                            
                            fs.saveFile('manifest.json', latestManifestContent, next);
                        });
                    });
                });
            });
        }, false);
    }
};


Updater.initialize();