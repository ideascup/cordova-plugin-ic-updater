# cordova-plugin-ic-updater

Automatically updates cordova application via server


# Installation

`cordova plugin add cordova-plugin-ic-updater --save --variable LOCATION=http://your_host.com/updater`


Configure updater in **www/updaterconfig.json**

```json
{
    "location": "http://updater.ideascup.me/demo",
    "manifestUrlPath": "/manifest.json",
    "indexFilePath": "index.html"
}
```

 - **location**: The root link to the server for downloading files. By default demo update server
 - **manifestUrlPath**: Path to manifest from the rootUrl
 - **indexFilePath**: File path to which will be app redirected

variables for `cordova plugin add`:

 - **LOCATION**
 - **MANIFEST_URL_PATH**
 - **INDEX_FILE_PATH**


# How it works

The plugin sends a request to the manifest file that is on update server via 
ajax. The manifest provides information about the latest version of the 
application and the files that need to be updated.

### manifest.json
```json
{
    "version": "hash",
    "files": ["index.html", "js/scripts.js"]
}
```


# Supported Platforms

 - Amazon Fire OS
 - Android
 - BlackBerry 10
 - Browser
 - Firefox OS
 - iOS
 - Windows Phone 7 and 8
 - Windows


# MIT License

Copyright (c) 2017 ideascup

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


